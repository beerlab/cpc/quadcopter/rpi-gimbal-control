ARG BASE_IMG

FROM ${BASE_IMG}

SHELL ["/bin/bash", "-ci"]
ENV DEBIAN_FRONTEND noninteractive

WORKDIR /workspace/ros_ws

COPY ./rpi-gimbal-control/requirements.txt src/rpi-gimbal-control/requirements.txt
RUN pip3 install RPi.GPIO

COPY . src/rpi-gimbal-control
