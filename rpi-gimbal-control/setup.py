import setuptools

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

DOCS_REQUIRE = [
    "sphinx~=6.2.1",
    "sphinx-autodoc-typehints~=1.23.0",
    "sphinx-rtd-theme~=1.2.0",
]

setuptools.setup(
    name='rpi-gimbal-control',
    version='0.0.1',
    author='TexnoMann',
    author_email='texnoman@itmo.ru',
    description='Simple python package for controlling gimbal using pwm',
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/beerlab/cpc/quadcopter/rpi-gimbal-control",
    project_urls={
        "Bug Tracker": "https://gitlab.com/beerlab/cpc/quadcopter/rpi-gimbal-control/-/issues",
    },
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    license='MIT',
    download_url = 'https://gitlab.com/beerlab/cpc/quadcopter/rpi-gimbal-control.git',
    include_package_data=True,
    package_dir={"": "src"},
    packages=setuptools.find_packages(where="src"),
    package_data={"": ["README.md", "LICENSE.txt"]},
    install_requires=[
        "jsonschema",
        "numpy >=1.20.0",
        "sympy",
        "lgpio"
   ],
   extras_require={
        "docs": DOCS_REQUIRE,
    },
)