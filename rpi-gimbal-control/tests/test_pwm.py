import RPi.GPIO as GPIO
from time import sleep

GPIO.setmode(GPIO.BCM)
GPIO.setup(12, GPIO.OUT)

pwm = GPIO.PWM(12, 2000)
pwm.start(0)

try:
    pwm.ChangeDutyCycle(50)
    input('F=50Hz, DC=50%. Press Enter...')
    pwm.ChangeDutyCycle(20)
    input('F=50Hz, DC=20%. Press Enter...')
    pwm.ChangeFrequency(2000)
    pwm.ChangeDutyCycle(80)
    input('F=1000Hz, DC=80%. Press Enter...')
    pwm.ChangeDutyCycle(10)
    input('F=1000Hz, DC=10%. Press Enter to exit...')
except KeyboardInterrupt:
    pass

pwm.stop()
GPIO.cleanup()
